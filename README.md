# seed

### setup:

`` $ find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs sudo rm -rf ``

`` $ chmod +x src/entrypoint.sh ``

`` $ docker-compose up ``

### development (only with tests):

### In another tab

`` $ docker-compose exec fastapi bash ``

`` $ $test ``

Check lint: `` $ $mypy $flake8 $bandit $safety ``

Check outdated: `` $ $outdated ``
