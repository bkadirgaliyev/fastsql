from typing import List

from fastapi import Depends, HTTPException, Query, APIRouter
from sqlmodel import Session, select

from marvel.models import (
    HeroRead, HeroCreate, HeroUpdate, Hero, HeroReadWithTeam,
    Team, TeamUpdate, TeamRead, TeamReadWithHeroes, TeamCreate
)
from core.db import get_session

router = APIRouter()
depends_on_session = Depends(get_session)


@router.post("/heroes/", response_model=HeroRead)
def create_hero(*, session: Session = depends_on_session, hero: HeroCreate):
    db_hero = Hero.from_orm(hero)
    session.add(db_hero)
    session.commit()
    session.refresh(db_hero)
    return db_hero


@router.get("/heroes/", response_model=List[HeroRead])
def read_heroes(
    *,
    session: Session = depends_on_session,
    offset: int = 0,
    limit: int = Query(default=100, lte=100),
):
    return session.exec(select(Hero).offset(offset).limit(limit)).all()


@router.get("/heroes/{hero_id}", response_model=HeroReadWithTeam)
def read_hero(*, session: Session = depends_on_session, hero_id: int):
    hero = session.get(Hero, hero_id)
    if not hero:
        raise HTTPException(status_code=404, detail="Hero not found")
    return hero


@router.patch("/heroes/{hero_id}", response_model=HeroRead)
def update_hero(
    *, session: Session = depends_on_session, hero_id: int, hero: HeroUpdate
):
    db_hero = session.get(Hero, hero_id)
    if not db_hero:
        raise HTTPException(status_code=404, detail="Hero not found")
    hero_data = hero.dict(exclude_unset=True)
    for key, value in hero_data.items():
        setattr(db_hero, key, value)
    session.add(db_hero)
    session.commit()
    session.refresh(db_hero)
    return db_hero


@router.delete("/heroes/{hero_id}")
def delete_hero(*, session: Session = depends_on_session, hero_id: int):

    hero = session.get(Hero, hero_id)
    if not hero:
        raise HTTPException(status_code=404, detail="Hero not found")
    session.delete(hero)
    session.commit()
    return {"ok": True}


@router.post("/teams/", response_model=TeamRead)
def create_team(*, session: Session = depends_on_session, team: TeamCreate):
    db_team = Team.from_orm(team)
    session.add(db_team)
    session.commit()
    session.refresh(db_team)
    return db_team


@router.get("/teams/", response_model=List[TeamRead])
def read_teams(
    *,
    session: Session = depends_on_session,
    offset: int = 0,
    limit: int = Query(default=100, lte=100),
):
    return session.exec(select(Team).offset(offset).limit(limit)).all()


@router.get("/teams/{team_id}", response_model=TeamReadWithHeroes)
def read_team(*, team_id: int, session: Session = depends_on_session):
    team = session.get(Team, team_id)
    if not team:
        raise HTTPException(status_code=404, detail="Team not found")
    return team


@router.patch("/teams/{team_id}", response_model=TeamRead)
def update_team(
    *,
    session: Session = depends_on_session,
    team_id: int,
    team: TeamUpdate,
):
    db_team = session.get(Team, team_id)
    if not db_team:
        raise HTTPException(status_code=404, detail="Team not found")
    team_data = team.dict(exclude_unset=True)
    for key, value in team_data.items():
        setattr(db_team, key, value)
    session.add(db_team)
    session.commit()
    session.refresh(db_team)
    return db_team


@router.delete("/teams/{team_id}")
def delete_team(*, session: Session = depends_on_session, team_id: int):
    team = session.get(Team, team_id)
    if not team:
        raise HTTPException(status_code=404, detail="Team not found")
    session.delete(team)
    session.commit()
    return {"ok": True}
