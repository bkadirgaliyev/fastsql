from pytest import fixture
from sqlalchemy.pool import StaticPool
from sqlmodel import create_engine, SQLModel, Session
from starlette.testclient import TestClient

from core.main import app
from core.db import get_session


@fixture
def session():
    engine = create_engine(
        "sqlite://", connect_args={"check_same_thread": False},
        poolclass=StaticPool,
    )
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        yield session


@fixture
def client(session: Session):
    def get_session_override():
        return session

    app.dependency_overrides[get_session] = get_session_override
    client = TestClient(app)
    yield client
    app.dependency_overrides.clear()
