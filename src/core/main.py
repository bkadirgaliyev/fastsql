from fastapi import FastAPI
from fastapi.responses import ORJSONResponse
from starlette.middleware.cors import CORSMiddleware

from marvel.views import router as heroes_router

main_app: FastAPI = FastAPI(default_response_class=ORJSONResponse)
main_app.add_middleware(
    middleware_class=CORSMiddleware,
    allow_origins=('*',), allow_credentials=True,
    allow_methods=('*',), allow_headers=('*',),
)
main_app.include_router(
    prefix='', router=heroes_router,
)
app = main_app
