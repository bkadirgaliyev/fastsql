from pydantic import (
    BaseSettings, PostgresDsn, AmqpDsn
)


class Settings(BaseSettings):
    DB_URL: PostgresDsn

    CELERY_BROKER_URL: AmqpDsn
    CELERY_RESULT_BACKEND: str


settings = Settings()
