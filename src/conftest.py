from typing import Tuple

pytest_plugins: Tuple[str, ...] = (
    'marvel.tests.fixtures',
)
