#!/bin/bash

wait_for () {
    for _ in `seq 0 100`; do
        (echo > /dev/tcp/$1/$2) >/dev/null 2>&1
        if [[ $? -eq 0 ]]; then
            echo "$1:$2 accepts connections!"
            break
        fi
        sleep 1
    done
}
just_sleep () {
  sleep 10
}
setup () {
  rm -rf celerybeat.pid celerybeat-schedule
}
setup
case "$PROCESS" in
"DEV_FASTAPI")
    just_sleep "${DB_HOST}" "${DB_PORT}"
    make migrate
    uvicorn core.main:app --reload --host 0.0.0.0 --port 8000
    ;;
"DEV_CELERY")
    wait_for "${BROKER_HOST}" "${BROKER_PORT}"
    celery -A core worker -B --loglevel=INFO --concurrency=1
    ;;
"FASTAPI")
    uvicorn core.main:app --host 0.0.0.0 --port 8000 \
    --proxy-headers --workers 4 --limit-max-requests 64
    ;;
"CELERY_SCHEDULER")
    celery -A core beat --loglevel=INFO
    ;;
"CELERY_CONSUMER")
    celery -A core worker --loglevel=INFO \
    --concurrency=12 --max-tasks-per-child=64
    ;;
*)
    echo "NO PROCESS SPECIFIED!"
    exit 1
    ;;
esac
